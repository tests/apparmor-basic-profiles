#!/bin/sh
# vim: set sts=4 sw=4 et tw=0 :
#

set -e

while [ "$#" -gt 0 ]; do
    case "$1" in
        (--debug)
            DEBUG=1
            set -x
            shift
            ;;
        (*)
            echo "unknown argument '$1'" >&2
            exit 2
            ;;
    esac
done

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/common/common.sh"

APPARMOR_PROFILES_DIRS="/etc/apparmor.d /etc/apparmor.d/local /etc/apparmor.d/tunables"
AUDIT_LOG="/var/log/audit/audit.log"

#########
# Setup #
#########
trap "setup_failure" EXIT

# For example, test_profiles_are_applied() has problems if you run it as
# root because programs behave differently if you run them as root instead
# of a normal user.
check_not_root

setup_success

###########
# Execute #
###########

_watch_audit_logs() {
    sudo tail -n 0 -f "${AUDIT_LOG}" > "$1"
}

# Check whether apparmor is enabled
test_apparmor_enabled() {
    if ! [ -d /etc/apparmor.d ]; then
        whine "AppArmor profile directory not found"
        return 1
    fi

    if ! [ -e /sys/module/apparmor ]; then
        whine "AppArmor module not loaded"
        return 1
    fi

    if ! [ -e /sys/kernel/security/apparmor/profiles ]; then
        whine "AppArmor profiles list not available"
        return 1
    fi

    if ! sudo grep . /sys/kernel/security/apparmor/profiles > /dev/null; then
        whine "Did not find anything in the AppArmor profile set"
        return 1
    fi

    if [ "$DEBUG" != 0 ]; then
        echo "# AppArmor profiles loaded:"
        sudo env LC_ALL=C sort /sys/kernel/security/apparmor/profiles | \
            sed -e 's/^/#  /'
    fi
}

# Check if profiles are being parsed correctly
test_profile_parsing() {
    local ret=0
    if ! /sbin/apparmor_parser -QKT "${TESTDIR}/test-profiles/usr.lib.valid-profile"; then
        whine "Failed to parse a valid profile!"
        ret=1
    fi

    if /sbin/apparmor_parser -QKT "${TESTDIR}/test-profiles/usr.lib.invalid-profile"; then
        whine "Successfully parsed an invalid profile!"
        ret=1
    fi

    return $ret
}

# Check if profiles have any syntax errors
test_profile_syntax() {
    local i
    local ret=0
    for i in ${APPARMOR_PROFILES_DIRS}; do
        if ! [ -d "$i" ]; then
            whine "Invalid apparmor profile directory: $i"
        fi
    done
    for i in $(find ${APPARMOR_PROFILES_DIRS} -maxdepth 1 -type f); do
        if [ -e "$i" ] && sudo apparmor_parser -Q "$i"; then
            :
        else
            whine "Failed to parse $i"
            ret=1
        fi
    done
    return $ret
}

# Check whether all the profiles are in complain mode
test_profiles_complain_mode() {
    PROFILED="$(sudo aa-status --profiled)"
    COMPLAINING="$(sudo aa-status --complaining)"
    if [ "${PROFILED}" != "${COMPLAINING}" ]; then
	whine "Not all profiles are in complain mode (${COMPLAINING}/${PROFILED})"
	return 1
    fi

    return 0
}

# Check whether profiles are being applied properly by comparing the audit 
# accesses against a "good" audit log for the same run
test_profiles_are_applied() {
    local each workdir
    workdir="$(create_temp_workdir)"
    for each in "${workdir}/profile-auditing"/*; do
        ## Run, and store the logs
        _watch_audit_logs "$each/audit.log" &
        $(cat $each/run)
        _sleep 3 && kill %1 && sync
        ## Extract logs
        # This hacky way of passing information via environment variables is a
        # temporary measure till we add option parsing to aa_log_extract_tokens.pl
        AA_PROFILE_WANTED=$(cat $each/aa-profile-wanted) aa_log_extract_tokens.pl AUDITING > "$each/filtered-audit.log" < "$each/audit.log"
        sync && _sleep 1
        ## Compare logs
        # For now, we do a straight compare, completely ignoring the 
        # possibility of out-of-order resource access. Later, we should write
        # a small function to do a set-compare to ensure that the same resources
        # are accessed each time, but ignoring the order.
        diff -u "$each/log" "$each/filtered-audit.log"
    done
}

trap "test_failure" EXIT

src_test_pass <<-EOF
test_apparmor_enabled
test_profile_parsing
test_profile_syntax
#test_profiles_complain_mode
#test_profiles_are_applied
EOF

test_success
